<?php

namespace diegotolentino\modelbase;

/**
 * The base connection class
 *
 * @author Diego Tolentino <diegotolentino@gmail.com>
 */
abstract class Db
{
    protected static $aDb = array();

    /**
     * Connect object
     *
     * @var \PDO
     */
    protected $dbh = null;

    /**
     * If any error has occurred {@link run()}
     *
     * @var bool
     */
    protected $hasError = false;

    /**
     * If will call the {@link end()} to commit/rollback the transaction
     *
     * @var unknown
     */
    protected $autocommit = false;

    /**
     * Create the object and conect db
     *
     * @param string $dsn
     *            Data Source Name
     * @param string $username
     *            User name
     * @param string $passwd
     *            Password
     * @throws Exception
     */
    protected function __construct ($dsn, $username, $passwd)
    {
        $this->dbh = new \PDO($dsn, $username, $passwd);
        $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->dbh->setAttribute(\PDO::ATTR_PERSISTENT, true);
    }

    /**
     * Called when destroys the object, and calls the {@link end()}, if it was
     * defined
     */
    public function __destruct ()
    {
        if ($this->autocommit) {
            $this->end();
        }
    }

    /**
     * Create new connection
     *
     * @param string $dsn
     *            Data Source Name
     * @param string $autocommit
     *            If automatic call begin/commit when create/destruct the
     *            connection
     *
     * @return Db
     */
    public static function connect ($dsn, $username, $passwd, $autocommit = true)
    {
        $oDb = new static($dsn, $username, $passwd);

        if ($autocommit) {
            $oDb->begin();
            $oDb->autocommit = $autocommit;
        }

        return static::$aDb[] = &$oDb;
    }

    /**
     * Return the last conected db, or Exception if not found
     *
     * @return Db
     * @throws Exception
     */
    final public static function getCurrent ()
    {
        if (! static::$aDb)
            throw new \PDOException('Nenhum banco conectado.');
        return end(static::$aDb);
    }

    /**
     * Start transaction
     *
     * @return void
     */
    public function begin ()
    {
        $this->dbh->beginTransaction();
    }

    /**
     * Close the transaction, if have erros call {@link rollback()},
     * else call {@link PDO::commit()}
     */
    public function end ()
    {
        if ($this->hasError) {
            $this->rollback();
        } else {
            $this->commit();
        }
    }

    /**
     * Rollback the transaction
     */
    public function rollback ()
    {
        $this->dbh->rollBack();
    }

    /**
     * Commit the transaction
     */
    public function commit ()
    {
        $this->dbh->commit();
    }

    /**
     * Run the sql and return the statement
     *
     * @return PDOStatement
     */
    public function run ($sql, $aParamBind = null)
    {
        try {
            /* prepare the statement */
            $statement = $this->dbh->prepare($sql);

            /* bind values if exists */
            if ($aParamBind) {
                foreach ($aParamBind as $key => $val) {
                    if ($val == null) {
                        $statement->bindValue(':' . $key, null, \PDO::PARAM_NULL);
                    } else {
                        $statement->bindValue(':' . $key, $val);
                    }
                }
            }

            $statement->execute();
        } catch (\Exception $e) {
            $aErroPDO = $statement->errorInfo();
            $sErroPDO = $aErroPDO[2] . PHP_EOL;
            $sErroPDO .= 'SQL: ' . $sql . PHP_EOL;
            $sErroPDO .= 'Parametros: ' . print_r($aParamBind, true) . PHP_EOL;
            $this->hasError = true;
            throw new \PDOException($sErroPDO);
        }

        return $statement;
    }

    /**
     * Get the last inserted item
     *
     * @var integer
     */
    abstract public function getLastInsertId ();
}