<?php

namespace diegotolentino\modelbase;

/**
 * Class to define basics Model funcions
 *
 * @author Diego Tolentino <diegotolentino@gmail.com>
 */
class ModelBase
{

    /**
     * Data base table of the class
     *
     * @var string
     */
    protected static $table_name = null;

    /**
     * Pk of the table, default 'id'
     *
     * @var string
     */
    protected static $table_pk = 'id';

    /**
     * Fields of the table, excluding Pk
     * Ex:
     * <code>
     * array('first_name', 'last_name');
     * </code>
     *
     * @var array
     */
    protected static $fields = array();

    /**
     * References the aggregated classes (foreignkeys) that the current class
     * depends
     *
     * @var array
     */
    protected static $aggregates = array();

    /**
     * Fields to check if filled before DB::save() or when call DB::isValid()
     *
     * <code>
     * array(
     * //field name required (will be show as "Last Name")
     * array('last_name'),
     *
     * //plus affordable name to show on mensage
     * array('email', 'name'=>'E-Mail'),
     *
     * //affordable mensage to show on require error
     * array('first_name', 'message' => 'é obrigatório')
     * );
     * </code>
     *
     * @link DB::save()
     * @link DB::isValid()
     * @var array
     */
    protected static $validates_presence_of = array();

    /**
     * Connect object
     *
     * @var Db
     */
    protected static $db = null;

    /**
     * If the record extend another(not auto generated pk, but inherity your
     * value)
     *
     * @var bool
     */
    protected static $inherit = false;

    /**
     * Array with values readed from db
     *
     * @var array
     */
    protected $aOriginalValue = array();

    /**
     * Store validate erros
     *
     * @var DbErrors
     */
    protected $errors = null;

    /**
     * Store columns information
     *
     * @var array
     */
    protected static $aFieldInfo = array();

    /**
     * Construct and initialize object with values
     *
     * @param string $aDefaults
     */
    public function __construct ($aDefaults = null)
    {
        /* set null for all fields */
        $this->{static::$table_pk} = null;
        foreach (static::$fields as $key) {
            $this->$key = null;
        }

        $this->setArray($aDefaults);

        $this->afterLoad();

        $this->errors = new DbErrors();
    }

    /**
     * Define what is the database conection to use
     *
     * @param Db $db
     */
    public static function setDb (Db $db)
    {
        self::$db = $db;
    }

    /**
     * Get the database conection
     *
     * @return Db
     */
    public static function getDb ()
    {
        return self::$db ? self::$db : Db::getCurrent();
    }

    /**
     * Find record by primary key
     *
     * @param integer|mixed $id
     * @return ModelBase
     */
    public static function find ($id)
    {
        return static::findOneBy(static::$table_pk, $id);
    }

    /**
     * Find all records that match with the key/val pair
     *
     * @param array|string $mField
     * @param array|mixed $mValue
     * @return ModelBase[]
     */
    public static function findBy ($mField, $mValue)
    {
        return static::findAllBy($mField, $mValue);
    }

    /**
     * Find one record by key/val pair
     *
     * @param array|string $mField
     * @param array|mixed $mValue
     * @return ModelBase
     */
    public static function findOneBy ($mField, $mValue, $orderBy = null)
    {
        $aResult = static::findAllBy($mField, $mValue, $orderBy, 1);
        return isset($aResult[0]) ? $aResult[0] : null;
    }

    /**
     * Find all records that match with the key/val pair, with the order by and
     * paginations possibility
     *
     * @param array|string $mField
     * @param array|mixed $mValue
     * @param string $orderBy
     * @param integer $limit
     * @param integer $offset
     * @return ModelBase[]
     */
    public static function findAllBy ($mField, $mValue, $orderBy = null, $limit = null, $offset = null)
    {
        /* se os parametros não forem um array, transforma */
        if ($mField && ! is_array($mField)) {
            $mField = array(
                    $mField);
            $mValue = array(
                    $mValue);
        }

        $sql = 'select * from ' . static::$table_name;

        $aFields = static::$fields;
        $aFields[] = static::$table_pk;
        if ($mField) {
            $sWhere = '';
            foreach ($mField as $key => $val) {
                if (in_array($val, $aFields)) {
                    $sWhere .= ($sWhere ? ' and ' : '');
                    if (strpos($mValue[$key], '%') !== false) {
                        $sWhere .= "lower($val) like lower(:$val)";
                    } else {
                        $sWhere .= "$val = :$val";
                    }
                }
            }
            if ($sWhere) {
                $sql .= " where $sWhere";
            }
        }

        if ($orderBy) {
            $sql .= " order by $orderBy ";
        }

        if ($limit) {
            $sql .= " limit $limit ";
        }

        $aParamBind = array();
        if ($mField) {
            $aParamBind = array_combine($mField, $mValue);
        }

        /* send debug info */
        self::debug($sql, $aParamBind);

        /* talk to db object to run the query */
        $oStatement = self::getDb()->run($sql, $aParamBind);

        /* return the fetch */
        return static::fetchToObject($oStatement);
    }

    /**
     * Find all records by $sWhere
     *
     * @param string $sWhere
     * @param string $orderby
     * @param integer $limit
     * @param integer $offset
     * @return ModelBase[]
     */
    public static function findByWhere ($sWhere, $orderBy = null, $limit = null, $offset = null)
    {
        $sql = 'select * from ' . static::$table_name;

        if ($sWhere) {
            $sql .= " where $sWhere ";
        }

        if ($limit) {
            $sql .= " limit $limit ";
        }

        if ($orderBy) {
            $sql .= " order by $orderBy ";
        }

        /* send debug info */
        self::debug($sql);

        /* pede ao objeto db associado para executar a query */
        $oStatement = self::getDb()->run($sql);

        /* faz o fetch devolvendo o resultado */
        return static::fetchToObject($oStatement);
    }

    /**
     * Fetch of statement, create the object of the children class and call
     * {@link afterLoad()}
     *
     * @param PDOStatement $oStatement
     * @return ModelBase[]
     */
    protected static function fetchToObject (\PDOStatement $oStatement)
    {
        /* if no result return null */
        if (! $oStatement->rowCount()) {
            return null;
        }

        /* PDO fech assoc */
        $aDados = $oStatement->fetchALL(\PDO::FETCH_ASSOC);

        /* get column's info */
        $class = get_called_class();
        if (isset(static::$aFieldInfo[$class])) {
            $aFieldInfo = static::$aFieldInfo[$class];
        } else {
            for($i = 0; $i <= count($aDados[0]); $i++) {
                $aux = $oStatement->getColumnMeta($i);
                $aFieldInfo[$aux['name']] = $aux;
            }
            static::$aFieldInfo[$class] = $aFieldInfo;
        }

        /* covers all records */
        foreach ($aDados as $aOldValues) {
            $o = new static();
            $o->{static::$table_pk} = $aOldValues[static::$table_pk];

            /* set the original values */
            $o->setOriginalValue($aOldValues);

            /* foreach to ignore non exposed values */
            foreach (static::$fields as $key) {
                $o->$key = $aOldValues[$key];
            }

            /* call afterLoad() */
            $o->afterLoad();
            $aResult[] = $o;

            unset($o);
        }

        return $aResult;
    }

    /**
     * Translate values from db to php like: '2013-01-01' => 1357005600
     *
     * @param mixed $mValue
     *            the db value
     * @param array $aInfo
     *            column info
     * @throws PDOException
     * @return mixed
     */
    protected static function parseFromDbToPhp ($mValue, $aInfo)
    {
        /*
         * @todo pensar uma forma de corrigir automaticamente tipos como SHORT
         * do mysql que não estão sendo reconhecidos automaticamente
         */
        return $mValue;
        switch (strtolower($aInfo['native_type'])) {
            case 'date' :
                if (strlen($mValue) == 10) {
                    $mValue = preg_replace('/[^0-9]/', ':', $mValue);
                    list($year, $month, $day) = explode(":", $mValue);
                    $mValue = mktime(0, 0, 0, $month, $day, $year);
                }
                break;
            case 'timestamp' :
                if ($mValue == '1969-12-31 20:59:58.000000') {
                    $mValue = - 1;
                } else {
                    $year = substr($mValue, 0, 4);
                    $month = substr($mValue, 5, 2);
                    $day = substr($mValue, 8, 2);
                    $hour = substr($mValue, 11, 2);
                    $min = substr($mValue, 14, 2);
                    $sec = substr($mValue, 17, 2);
                    $mValue = mktime($hour, $min, $sec, $month, $day, $year);
                }
                break;
            case 'datetime' :
                $mValue = preg_replace("/[' '|-]/i", ":", $mValue);
                list($year, $month, $day, $hour, $min, $sec) = explode(":", $mValue);
                $mValue = mktime($hour, $min, $sec, $month, $day, $year);
                break;
            case 'string' :
            case 'char' :
            case 'bpchar' :
            case 'var_string' :
            case 'varchar' :
            case 'blob' :
            case 'text' :
            case 'long' :
                $s = array(
                        '\\\\',
                        '\\\"',
                        '\\"',
                        '\\\'',
                        "\\'");
                $r = array(
                        '\\',
                        '\"',
                        '"',
                        '\'',
                        "'");
                $mValue = str_replace($s, $r, $mValue);
                break;
            case 'bool' :
                $mValue = (in_array($mValue, array(
                        't',
                        'true',
                        'y',
                        'yes',
                        '1')) ? TRUE : FALSE);
                settype($mValue, 'bool');
                break;
            case 'int2' :
            case 'int4' :
            case 'int8' :
            case 'integer' :
                settype($mValue, 'int');
                break;
            case 'numeric' :
            case 'time':
                                /* não faz nada */
                                break;
            default :
                throw new \PDOException('Tipo do campo não suportado.<br>Valor: ' . $mValue . '<br>Info: ' . print_r($aInfo, true));
        }
        return $mValue;
    }

    /**
     * Set the original values
     *
     * @param array $aValues
     */
    protected function setOriginalValue ($aValues)
    {
        $this->aOriginalValue = $aValues;
    }

    /**
     * Get the original value from one key
     *
     * @param string $key
     * @return mixed
     */
    protected function getOriginalValue ($key)
    {
        return isset($this->aOriginalValue[$key]) ? $this->aOriginalValue[$key] : null;
    }

    /**
     * Function had called after load the record, to make the jobs like load
     * aggregates
     *
     * @return void
     */
    protected function afterLoad ()
    {
        foreach (static::$aggregates as $val) {
            /* if the fk was defined, load the value from aggregate */
            if ($fk = $this->getOriginalValue($val['foreign_key'])) {
                $o = $val['class']::find($fk);
            } else {
                $o = new $val['class']();
            }

            /* to array in aggregate, to work with him as array */
            $this->$val['index'] = $o->toArray();
        }
    }

    /**
     * Save the record and return the primary key
     *
     * @throws Exception
     * @return integer
     */
    public function save ()
    {
        /* call the before save method */
        $this->beforeSave();

        /* if the record is not valid, throw exception */
        if (! $this->isValid()) {
            throw new \Exception(join("\n", $this->errors->getFullMessages()));
        }

        /*
         * if model is inheritd (pk extends another), check the record's
         * existence
         */
        if (static::$inherit && $this->{static::$table_pk} != '') {
            $sql = "select * from " . static::$table_name . ' where ' . static::$table_pk . '=' . $this->{static::$table_pk};
            $statement = static::getDb()->run($sql);
            $newReg = $statement->rowCount() == 0;
        } else {
            $newReg = ! (isset($this->{static::$table_pk}) && $this->{static::$table_pk} != '');
        }

        /* start build the field=value association to persist */
        $fields = array();
        $values = array();
        $aParamBind = array();

        /* the exposed fields */
        foreach (static::$fields as $key) {
            if ($key != static::$table_pk) {
                if (! $newReg) {
                    $fields[] = $key . '= :' . $key;
                } else {
                    $fields[] = $key;
                    $values[] = ':' . $key;
                }
                $aParamBind[$key] = $this->$key;
            }
        }

        /* if inherit add the pk to field list */
        if (static::$inherit && $newReg) {
            $fields[] = static::$table_pk;
            $values[] = ':' . static::$table_pk;
            $aParamBind[static::$table_pk] = $this->{static::$table_pk};
        }

        /* the foreignkeys */
        foreach (static::$aggregates as $aggregate) {
            $key = $aggregate['foreign_key'];
            $table_pk = $aggregate['class']::getTablePk();
            $val = $this->{$aggregate['index']}[$table_pk];
            if (! $newReg) {
                $fields[] = $key . '= :' . $key;
            } else {
                $fields[] = $key;
                $values[] = ':' . $key;
            }
            $aParamBind[$key] = $val;
        }

        /* check if the pk is defined */
        if (! $newReg) {
            $sql = 'UPDATE ' . static::$table_name;
            $sql .= ' SET ' . join(', ', $fields);
            $sql .= ' WHERE ' . static::$table_pk . '= :' . static::$table_pk;
            $aParamBind[static::$table_pk] = $this->{static::$table_pk};
        } else {
            $sql = 'INSERT INTO ' . static::$table_name . '(' . join(', ', $fields) . ') values (' . join(', ', $values) . ')';
            $newReg = true;
        }

        /* send debug info */
        self::debug($sql, $aParamBind);

        /* run sql */
        $statement = static::getDb()->run($sql, $aParamBind);

        /* bind the generated pk value */
        if ($newReg && ! static::$inherit) {
            $this->{static::$table_pk} = self::getDb()->getLastInsertId();
        }

        /* return the pk value */
        return $this->{static::$table_pk};
    }

    /**
     * Function had called before save function, to make the jobs like save
     * aggregates
     *
     * @return void
     */
    protected function beforeSave ()
    {
        foreach (static::$aggregates as $val) {
            try {
                /* create the aggregate object */
                $o = new $val['class']();

                /* set the values */
                $o->setArray($this->{$val['index']});

                if ($o->isValid()) {
                    /* persiste the values */
                    $o->save();

                    /* update the aggregate value in master object */
                    $this->{$val['index']} = $o->toArray();
                } else {
                    /* check if the presence of aggregate is required */
                    if ($val['required'] || ! $o->isNull()) {
                        throw new \Exception(json_encode($o->errors->getErrorByField()));
                    }
                }
            } catch (\Exception $e) {
                /*
                 * se ele não conseguir decodificar a mensagem quer dizer que
                 * ela veio do save() e não um array de erros
                 */
                if (! $aMensagem = json_decode($e->getMessage(), true)) {
                    $aMensagem = $e->getMessage();
                }
                throw new \Exception(json_encode(array(
                        $val['index'] => $aMensagem)));
            }
        }
    }

    /**
     *
     * @todo Terminar: só é executada quando cria um registro novo
     */
    protected function beforeCreate ()
    {
    }

    /**
     *
     * @todo Terminar: só é executada quando cria um registro novo
     */
    protected function afterCreate ()
    {
    }

    /**
     *
     * @todo implementar função afterSave()
     */
    protected function afterSave ()
    {
    }

    /**
     * Delete de current row and return the number of affected rows
     *
     * @throws Exception
     * @return integer
     */
    public function delete ()
    {
        if (! isset($this->{static::$table_pk}) || ! $this->{static::$table_pk}) {
            throw new \Exception(static::$table_pk . '.' . static::$table_pk . ' not is defined.');
        }

        $sql = 'DELETE FROM ' . static::$table_name . ' WHERE ' . static::$table_pk . '=:' . static::$table_pk;

        $aParamBind = array(
                static::$table_pk => $this->{static::$table_pk});

        /* send debug info */
        self::debug($sql, $aParamBind);

        /* talk to db object to run the query */
        $oStatement = self::getDb()->run($sql, $aParamBind);

        /* return the number of affected rows */
        return $oStatement->rowCount();
    }

    /**
     *
     * @todo implementar função beforeDelete()
     */
    protected function beforeDelete ()
    {
    }

    /**
     *
     * @todo implementar função afterDelete()
     */
    protected function afterDelete ()
    {
    }

    /**
     * Check if the record is valid and return bool, you can view the info using
     * $o->errors->* functions
     *
     * @return boolean
     */
    public function isValid ()
    {
        $this->errors->reset();

        /* validate required fields */
        foreach (static::$validates_presence_of as $aValidate) {
            if (! isset($this->$aValidate[0]) || ! $this->$aValidate[0]) {
                $sField = $aValidate[0];

                /* Define field name */
                if (isset($aValidate['name'])) {
                    $sLabel = $aValidate['name'];
                } else {
                    $sLabel = ucwords(str_replace('_', ' ', $aValidate[0]));
                }

                /* Define mensage */
                if (isset($aValidate['message'])) {
                    $sMsg = $aValidate['message'];
                } else {
                    $sMsg = 'is required';
                }

                $this->errors->add($sField, $sLabel, $sMsg);
            }
        }

        /* validate user rules */
        $this->validate();

        return $this->errors->count() == 0;
    }

    /**
     * Check all values of object, and get back true if all
     * is null, otherwise, return false
     *
     * @return boolean
     */
    public function isNull ()
    {
        foreach (static::$fields as $val) {
            if (isset($this->$val) && ! is_null($this->$val)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Display debug info
     *
     * @param string $string
     */
    static protected function debug ($string, $aValores = array())
    {
        /**
         * check if show debug info
         */
        if (isset($_REQUEST['debug']) && $_REQUEST['debug']) {
            $aux = debug_backtrace();
            $sClassFunc = $aux[1]['class'] . ':' . $aux[1]['function'];
            $sFileLine = substr(strrchr($aux[1]['file'], DIRECTORY_SEPARATOR), 1) . ':' . $aux[1]['line'];
            echo "\n<br><b>Debug - $sClassFunc ($sFileLine)</b><br>";
            echo "<pre>\n\n\t";
            echo wordwrap($string, 80) . "\n\n";
            if ($aValores) {
                echo print_r($aValores, true) . "\n\n";
            }
            echo "</pre><br>\n\n";
        }
    }

    /**
     * Data bind sem nenhuma restrição
     *
     * @param array $aValues
     * @return ModelBase
     */
    public function setArray ($aValues = array())
    {
        /* if have defaults, set the default value for fields */
        if ($aValues) {
            foreach ($aValues as $key => $val) {
                $this->$key = $val;
            }
        }
        return $this;
    }

    /**
     * Turn the object as a printable array, with all exposed param(including
     * the aggregates)
     *
     * @return array
     */
    public function toArray ()
    {
        /* return array */
        $aReturn = array();

        /* set the pk */
        $aReturn[static::$table_pk] = $this->{static::$table_pk};

        /* set the exposed fiedls */
        foreach (static::$fields as $key) {
            $aReturn[$key] = $this->$key;
        }

        /* set the aggregates objects */
        foreach (static::$aggregates as $val) {
            $aReturn[$val['index']] = $this->{$val['index']};
        }

        return $aReturn;
    }

    /**
     * Return the pk name
     *
     * @return string
     */
    public static function getTablePk ()
    {
        return static::$table_pk;
    }

    /**
     * Pega o objeto que contem os erros
     *
     * @return DbErrors
     */
    public function getErros ()
    {
        return $this->errors;
    }
}