<?php

namespace diegotolentino\modelbase;

/**
 * Extends the connection class to work with Postgres
 *
 * @author Diego Tolentino <diegotolentino@gmail.com>
 */
class DbPgsql extends Db
{

    /**
     * Get the last inserted item
     *
     * @var integer
     */
    public function getLastInsertId ()
    {
        /* prepare the statement */
        $statement = $this->dbh->prepare("select lastval() as last_id;");
        $statement->execute();
        $aDados = $statement->fetch(\PDO::FETCH_ASSOC);
        return $aDados['last_id'];
    }
}