<?php

namespace diegotolentino\modelbase;

if (! defined('PHP_VERSION_ID') || PHP_VERSION_ID < 50300) {
    die('Requires PHP 5.3 or higher');
}

/**
 * Class to store the validation erros
 *
 * @author Diego Tolentino <diegotolentino@gmail.com>
 */
class DbErrors
{
    protected $aErrors = null;

    function __construct ()
    {
        $this->errors = array();
    }

    /**
     * Add info to error array
     *
     * @param string $field
     * @param string $msg
     */
    function add ($sField, $sLabel, $sMsg)
    {
        $this->aErrors[$sField][] = array(ucfirst($sLabel), $sMsg);
    }

    /**
     * Return array with the full field+errors info
     *
     * @return array
     */
    function getFullMessages ()
    {
        $sResult = array();
        if ($this->aErrors) {
            foreach ($this->aErrors as $field => $aErros) {
                foreach ($aErros as $aError) {
                    $sResult[] = $aError[0] . ': ' . $aError[1];
                }
            }
        }
        return $sResult;
    }

    /**
     * Return array of errors by fields
     *
     * @return array[array[]]
     */
    public function getErrorByField ()
    {
        return $this->aErrors;
    }

    /**
     * Reset the error info
     */
    function reset ()
    {
        $this->aErrors = array();
    }

    /**
     * Return the count of errors
     *
     * @return number
     */
    function count ()
    {
        return count($this->aErrors);
    }
}