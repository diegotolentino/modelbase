<?php

namespace diegotolentino\modelbase;

/**
 * Extends the connection class to work with Mysql
 *
 * @author Diego Tolentino <diegotolentino@gmail.com>
 */
class DbMysql extends Db
{

    /**
     * Create the object and conect db
     *
     * @param string $dsn
     *            Data Source Name
     * @param string $username
     *            User name
     * @param string $passwd
     *            Password
     * @throws Exception
     */
    protected function __construct ($dsn, $username, $passwd)
    {
        parent::__construct($dsn, $username, $passwd);
        $this->run('set innodb_lock_wait_timeout=600');
    }

    /**
     * Get the last inserted item
     *
     * @var integer
     */
    public function getLastInsertId ()
    {
        return $this->dbh->lastInsertId();
    }
}